'use strict';
import util from 'better-sqlite3/lib/util'


class NotImplementedError extends Error {}
const throwNotImplementedError = () => {
  console.log('Not implemented');
  throw new NotImplementedError()
};

function Database() {

  Object.defineProperties(this, {
    [util.cppdb]: env.DB,
    ...wrappers.getters,
  });
}

const wrappers = require('better-sqlite3/lib/methods/wrappers');
Database.prototype.prepare = (sql) => sql;
Database.prototype.get = function (...rest) {
  return env.DB.get(...rest)
};
Database.prototype.transaction = require('better-sqlite3/lib/methods/transaction');
Database.prototype.pragma = require('better-sqlite3/lib/methods/pragma');
Database.prototype.backup = throwNotImplementedError //require('better-sqlite3/lib/methods/backup');
Database.prototype.serialize = require('better-sqlite3/lib/methods/serialize');
Database.prototype.function = require('better-sqlite3/lib/methods/function');
Database.prototype.aggregate = require('better-sqlite3/lib/methods/aggregate');
Database.prototype.table = require('better-sqlite3/lib/methods/table');
Database.prototype.loadExtension = wrappers.loadExtension;
Database.prototype.exec = wrappers.exec;
Database.prototype.close = wrappers.close;
Database.prototype.defaultSafeIntegers = wrappers.defaultSafeIntegers;
Database.prototype.unsafeMode = wrappers.unsafeMode;
Database.prototype[util.inspect] = require('better-sqlite3/lib/methods/inspect');


module.exports = Database