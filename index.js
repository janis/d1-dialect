import SqlLiteDialect from 'knex/lib/dialects/better-sqlite3/index.js';

export default class D1Dialect extends SqlLiteDialect {
  _driver () {
    return require('./driver.js')
  }

  async _query(connection, obj) {
    if (!obj.sql) throw new Error('The query is empty');

    if (!connection) {
      throw new Error('No connection provided');
    }

    const { method } = obj;
    let callMethod;
    switch (method) {
      case 'get':
        callMethod = 'get';
        break;
      case 'insert':
      case 'update':
      case 'counter':
      case 'del':
        callMethod = 'run';
        break;
      default:
        callMethod = 'all';
    }

    if (!connection[callMethod]) {
      throw new Error(`Error calling ${callMethod} on connection.`)
    }

    const statement = connection.prepare(obj.sql);
    const bindings = this._formatBindings(obj.bindings);

    const response = await connection[callMethod](statement, bindings);

    obj.response = response;
    obj.context = {
      lastID: response.lastInsertRowid,
      changes: response.changes,
    };

    return obj;
  }
}
