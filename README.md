# d1-dialect

A Dialect for Cloudflare's D1 to be used with Knex, based on `better-sqlite3`.

Since the D1 API has not been published yet this is only a scaffolding.

### Usage

```js
import d1Dialect from 'd1-dialect';

const knex = new Knex({
  client: d1Dialect,
  driverName: 'd1',
  connection: {}
})
```

### Issues

This library is experimental, contributions are welcome!

To open an issue, go to https://gitlab.com/janis/d1-dialect/-/issues